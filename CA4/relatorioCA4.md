

CA4 - DeVops Class Assignment 4 - readme file
===================

Criação de Contentores com o Docker
-----

O Docker é uma ferramenta projetada para facilitar a criação, implementação e execução de aplicações usando containers. Os containers permitem empacotar uma aplicação com todas os componentes necessários, como bibliotecas e outras dependências. Ao fazer isto, o developer pode ter certeza de que a sua aplicação será executado em qualquer outra máquina, independentemente das configurações personalizadas que a máquina possa ter e que possam diferir da máquina usada para escrever e testar o código.

Neste CA4 iremos criar com o Docker dois containers para fazer correr o programa do CA2-part2, o tutorial spring boot application.

Para isto foi feito o clone da aplicação em docker de referência existente no repositório **https://bitbucket.org/atb/docker-compose-spring-tut-demo/**.

Copiando todos os ficheiros clonados para o meu repositório para a pasta **CA4**, fiz as alterações a baixo no **Dockerfile** presente na pasta **web** para que foce construido um container web que inicializa-se o projecto contido em **CA2-parte2**:



      FROM tomcat

      RUN apt-get update -y

      RUN apt-get install -f

      RUN apt-get install git -y

      RUN apt-get install nodejs -y

      RUN apt-get install npm -y

      RUN mkdir -p /tmp/build

      WORKDIR /tmp/build/

      RUN git clone https://luisfvfigueiredo@bitbucket.org/luisfvfigueiredo/devops-19-20-b-1091064.git

      WORKDIR /tmp/build/devops-19-20-b-1091064/CA2/part2/demo/

      RUN ./gradlew clean build

      RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

      EXPOSE 8080

Para termos a rede disponível com as mesmas configurações do Docker de exemplo temos que fazer o seguinte comando:

    docker network rm docker-compose-spring-tut-demo_default


Com estas alterações podemos agora aceder à pasta **CA4** e activar os containers:

    docker-compose build


    docker-compose up



Com isto comcluído conseguimos aceder ao font end no browser através do endereço:

    http://localhost:8080/demo-0.0.1-SNAPSHOT/

E com o endereço a baixo para aceder à consola da base de dados (usar **jdbc:h2:tcp://192.168.33.11:9092/./jpadb** no campo JDBC URL e **luis** como username) :

    http://localhost:8082
    
    
Criação de um repositorio de Dockers no Docker Hub
-----  
Após criar uma conta em https://hub.docker.com fiz o login na minha linha de comandos usando:

    docker login

Para saber as imagens que tinha fiz:

    docker images

Com o nome das imagens web e db fiz o push dos dockers para o repositório do Docker Hub, utilizando os comandos:

    docker tag f0f3ec473c79 luisfvfigueiredo/ca4_web:web

    docker push luisfvfigueiredo/ca4_web



    docker tag 1b9172fcfe60 luisfvfigueiredo/ca4_db:db

    docker push luisfvfigueiredo/ca4_db

Este repositorio pode ser agora consultado em https://hub.docker.com/u/luisfvfigueiredo
