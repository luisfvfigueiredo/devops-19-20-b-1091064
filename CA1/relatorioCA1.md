CA1 - DeVops Class Assignment 1 - readme file
---
* verifiquei se havia algum tag da versão

MacBook-Pro-de-Joana-2:CA1 Luis$ git tag

* adicionei o tag da nova versão v1.2.0

MacBook-Pro-de-Joana-2:CA1 Luis$ git tag -a v1.2.0

* verifiquei se a versão ficou guardada

MacBook-Pro-de-Joana-2:CA1 Luis$ git show v1.2.0
tag v1.2.0
Tagger: Luis Figueiredo <1091064@isep.ipp.pt>
Date:   Thu Mar 5 12:07:54 2020 +0000

initial version

commit a0a40ee4a15bce0a4faca99d52b0e89631fc02c2 (HEAD -> master, tag: v1.2.0, origin/master)
Author: Luis Figueiredo <1091064@isep.ipp.pt>
Date:   Thu Feb 27 13:09:13 2020 +0000

    change react

* fiz o push da alteração para o repositório

MacBook-Pro-de-Joana-2:CA1 Luis$ git push origin v1.2.0

.......................................................

* Criei um novo Branch chamado "email-field" e em seguida acrescentei o atributo "email" à class Emplyee;
* Criei uma classe de testes para Employee e fiz os testes ao seu construtor.

De seguida vai ser feito o update do Master com o Branch "email-field". e adicionado um novo Tag para a versão 1.3.0

MBP-de-Joana-2:CA1 Luis$ git tag -a v1.3.0 -m "my version 1.3.0"

MBP-de-Joana-2:CA1 Luis$ git push origin v1.3.0

.........................................................gi

* Criei um novo Branch chamado "fix-invalid-email" e em seguida acrescentei ao set do email as condições de nao poder ser nulo, vazio e ter que cumprir com o formato de email ou seja ter @ e .xpto;
* Na classe de testes de Employee acrescenteio os testes de verificação do formato de email

De seguida vai ser feito o update do Master com o Branch "fix-invalid-email". e adicionado um novo Tag para a versão 1.3.1

Switched to branch 'master'
Your branch is up to date with 'origin/master'.
MBP-de-Joana-2:CA1 Luis$ git merge fix-invalid-email
Already up to date.


MBP-de-Joana-2:CA1 Luis$ git tag -a v1.3.0 -m "my version 1.3.0"

MBP-de-Joana-2:CA1 Luis$ git push origin v1.3.0

-----------------
finalmente vai ser adicionada uma nova tag "ca1"

---------------------------------------
---------------------------------------

Na versão alternativa instalei o Mercurial atravez da linha de comandos e usei o helixteamhub para servir de repositorio.

o link para o repositório é o seguinte:

https://1091064isepipppt@helixteamhub.cloud/sticky-picture-1218/projects/ca1_alt/repositories/mercurial/ca1_alt
