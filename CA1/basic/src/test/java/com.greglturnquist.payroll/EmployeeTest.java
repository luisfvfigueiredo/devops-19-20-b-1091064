package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;


class EmployeeTest {
    @DisplayName("create employee - Happy Path")
    @Test
    void createemployeeHappyPath() {
        //Arrange//Act//Assert
        new Employee("Luis","Figueiredo","programador","Switch","luis@gmail.com");
    }

    @DisplayName("employee with email - Null")
    @Test
    void setIsEmailValidNull() {

        //Arrange//Act//Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee("Luis","Figueiredo","programador","Switch",null);
        });
    }

    @DisplayName("employee with email - Empty")
    @Test
    void setIsEmailValidEmpty() {

        //Arrange//Act//Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Luis","Figueiredo","programador","Switch", "");
        });
    }

    @DisplayName("employee with email - without @")
    @Test
    void setIsEmailValidWithoutArroba() {

        //Arrange//Act//Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Luis","Figueiredo","programador","Switch","luisgmail.com");
        });
    }

}