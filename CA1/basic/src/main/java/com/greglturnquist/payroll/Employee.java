/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String department;
    private String email;

    private Employee() {
    }

    public Employee(String firstName, String lastName, String description, String department, String email) {
        setFirstName(firstName);
        setLastName(lastName);
        setDescription(description);
        setDepartment(department);
        setIsEmailValid(email);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(department, employee.department);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, description, department);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName == null) {
            throw new NullPointerException("first name cannot be null");
        }
        if (firstName.trim().isEmpty()) {
            throw new IllegalArgumentException("first name is invalid");
        } else this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName == null) {
            throw new NullPointerException("last name cannot be null");
        }
        if (lastName.trim().isEmpty()) {
            throw new IllegalArgumentException("last name is invalid");
        } else this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description == null) {
            throw new NullPointerException("description cannot be null");
        }
        if (description.trim().isEmpty()) {
            throw new IllegalArgumentException("description is invalid");
        } else this.description = description;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        if (department == null) {
            throw new NullPointerException("department cannot be null");
        }
        if (department.trim().isEmpty()) {
            throw new IllegalArgumentException("department is invalid");
        } else this.department = department;
    }

    public String getEmail() {
        return email;
    }

    public void setIsEmailValid(String email) {
        if (email == null) {
            throw new NullPointerException("e-mail cannot be null");
        }
        if (email.trim().isEmpty()) {
            throw new IllegalArgumentException("e-mail is invalid");
        }

        Pattern regexPattern = Pattern.compile("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$");
        Matcher regMatcher = regexPattern.matcher(email);
        if (regMatcher.matches()) {
            this.email = email;
        } else {
            throw new IllegalArgumentException("e-mail is invalid");
        }
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", department='" + department + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
// end::code[]
