

CA3 - DeVops Class Assignment 3 part 1 - readme file
===================

Criação de Multi Máquinas virtuais com o Vagrant
-----
Nesta segunda parte iremos criar com o Vagrant duas máquinas virtuais para fazer correr o programa do CA2-part2, o tutorial spring boot application.
Para isto foi gerada uma Vagrantfile de referência, fazendo o **vagrant up** do projecto pré-criado, existente no repositório **https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/**.

Copiando a **Vagrantfile** para o meu repositório para a pasta **CA3/part2** comecei por fazer alterações (a baixo) à mesma, para que ao fazer **Vagrant up** ela inicializa-se o projecto da pasta **CA2/part2**.



      git clone https://luisfvfigueiredo@bitbucket.org/luisfvfigueiredo/devops-19-20-b-1091064.git
      cd devops-19-20-b-1091064/CA2/part2/demo
      sudo ./gradlew clean build
      # To deploy the war file to tomcat8 do the following command:
      sudo cp build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

Para que a aplicação fica-se funcional tiveram que ser alterados também todos os ficheiros em **src/main/java/com/basic/** para **package com.basic;**, e os conteudos dos ficheiros: **src/main/java/com/basic/ServletInitializer.java**, **build.gradle** , **src/main/resources/application.properties** , **src/main/js/app.js** e **package.json** . vou a baixo colocar o seu conteúdo, ficheiro a ficheiro, seguindo os commits que o professor fez em **https://bitbucket.org/atb/tut-basic-gradle/commits/**:


    
**ServletInitializer.java**
    
    package com.basic;
    
    import org.springframework.boot.builder.SpringApplicationBuilder;
    import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
    
    public class ServletInitializer extends SpringBootServletInitializer {
    
        @Override
        protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
            return application.sources(ReactAndSpringDataRestApplication.class);
        }
    
    }
    
**build.gradle**

    plugins {
        id 'org.springframework.boot' version '2.2.6.RELEASE'
        id 'io.spring.dependency-management' version '1.0.9.RELEASE'
        id 'java'
      id 'org.siouan.frontend' version '1.4.1'
        id 'war'
    }
    
    group = 'com.example'
    version = '0.0.1-SNAPSHOT'
    sourceCompatibility = '1.8'
    
    repositories {
        mavenCentral()
    }
    
    dependencies {
        implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
        implementation 'org.springframework.boot:spring-boot-starter-data-rest'
        implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
        runtimeOnly 'com.h2database:h2'
        testImplementation('org.springframework.boot:spring-boot-starter-test') {
            exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
        }
        // To support war file for deploying to tomcat
        providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
    }
    
    test {
        useJUnitPlatform()
    }
    
    frontend {
    nodeVersion = "12.13.1"
    assembleScript = "run webpack"
    }
    
    task copyTask(type:Copy){
        from 'build/libs'
        into 'dist'
    }
    
    task delete (type:Delete,dependsOn:clean) {
        clean.doFirst {
            delete 'src/main/resources/static/built'
        }
    }

**src/main/resources/application.properties**
    
    server.servlet.context-path=/basic-0.0.1-SNAPSHOT
    spring.data.rest.base-path=/api
    #spring.datasource.url=jdbc:h2:mem:jpadb
    # In the following settings the h2 file is created in /home/vagrant folder
    spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    spring.datasource.driverClassName=org.h2.Driver
    spring.datasource.username=luis
    spring.datasource.password=
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    # So that spring will no drop de database on every execution.
    spring.jpa.hibernate.ddl-auto=update
    spring.h2.console.enabled=true
    spring.h2.console.path=/h2-console
    spring.h2.console.settings.web-allow-others=true
    
**src/main/js/app.js**

    'use strict';
    
    // tag::vars[]
    const React = require('react'); // <1>
    const ReactDOM = require('react-dom'); // <2>
    const client = require('./client'); // <3>
    // end::vars[]
    
    // tag::app[]
    class App extends React.Component { // <1>
    
        constructor(props) {
            super(props);
            this.state = {employees: []};
        }
    
        componentDidMount() { // <2>
            client({method: 'GET', path: '/demo-0.0.1-SNAPSHOT/api/employees'}).done(response => {
                this.setState({employees: response.entity._embedded.employees});
            });
        }
    
        render() { // <3>
            return (
                <EmployeeList employees={this.state.employees}/>
            )
        }
    }
    // end::app[]
    
    // tag::employee-list[]
    class EmployeeList extends React.Component{
        render() {
            const employees = this.props.employees.map(employee =>
                <Employee key={employee._links.self.href} employee={employee}/>
            );
            return (
                <table>
                    <tbody>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Description</th>
                        </tr>
                        {employees}
                    </tbody>
                </table>
            )
        }
    }
    // end::employee-list[]
    
    // tag::employee[]
    class Employee extends React.Component{
        render() {
            return (
                <tr>
                    <td>{this.props.employee.firstName}</td>
                    <td>{this.props.employee.lastName}</td>
                    <td>{this.props.employee.description}</td>
                </tr>
            )
        }
    }
    // end::employee[]
    
    // tag::render[]
    ReactDOM.render(
        <App />,
        document.getElementById('react')
    )
    // end::render[]

**src/main/resources/templates/index.html**

    <!DOCTYPE html>
    <html xmlns:th="https://www.thymeleaf.org">
    <head lang="en">
        <meta charset="UTF-8"/>
        <title>ReactJS + Spring Data REST</title>
        <link rel="stylesheet" href="main.css" />
    </head>
    <body>
    
        <div id="react"></div>
    
        <script src="built/bundle.js"></script>
    
    </body>
    
    </html>
    
**package.json**

    {
      "name": "spring-data-rest-and-reactjs",
      "version": "0.1.0",
      "description": "Demo of ReactJS + Spring Data REST",
      "repository": {
        "type": "git",
        "url": "git@github.com:spring-guides/tut-react-and-spring-data-rest.git"
      },
      "keywords": [
        "rest",
        "hateoas",
        "spring",
        "data",
        "react"
      ],
      "author": "Greg L. Turnquist",
      "license": "Apache-2.0",
      "bugs": {
        "url": "https://github.com/spring-guides/tut-react-and-spring-data-rest/issues"
      },
      "homepage": "https://github.com/spring-guides/tut-react-and-spring-data-rest",
      "dependencies": {
        "react": "^16.5.2",
        "react-dom": "^16.5.2",
        "rest": "^1.3.1"
      },
      "scripts": {
        "watch": "webpack --watch -d",
        "webpack": "webpack"
      },
      "devDependencies": {
        "@babel/core": "^7.1.0",
        "@babel/preset-env": "^7.1.0",
        "@babel/preset-react": "^7.0.0",
        "babel-loader": "^8.0.2",
        "webpack": "^4.19.1",
        "webpack-cli": "^3.1.0"
      }
    }
    
Após estas alterações já podemos fazer, na pasta **CA3/part**, o comando:

    vagrant up --provision
    
    vagrant ssh web
    
e depois das duas VM estarem a correr podemos executar o programa com:

    ./gradlew build

Com isto comcluído conseguimos aceder ao font end no browser através do endereço:

    http://localhost:8080/basic-0.0.1-SNAPSHOT/

e o endereço a baixo para aceder à consola(usar **jdbc:h2:tcp://192.168.33.11:9092/./jpadb** no campo JDBC URL) :

    http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
    
    