

CA3 - DeVops Class Assignment 3 part 1 - readme file
===================

Nesta terceira parte vai ser criada uma maquina virtual a partir do Vagrant e inicializados os projectos do CA2-part1 e part2.

Criação de Máquina virtual com o Vagrant
-----
Já com a Virtualbox da Oracle instalada, numa localização à escolha vamos criar uma pasta na linha de comando e entrar na mesma:

    mkdir vagrant-project-1

    cd vagrant-project-1/

depois vamos iniciar uma box com o Sistema operativo neste caso:

    vagrant init envimation/ubuntu-xenial

após instalar o OS podemos iniciar a máquina virtual usando:

    vagrant up
e iniciar uma sessão com (sempre que se faz este comando numa linha de comando nova é aberta uma nova sessão):

    vagrant ssh  

se quisermos finalizar a sessão basta digitar:

    exit

e para desligar a máquina virtual:

    vagrant halt

No vagrantfile criado na pasta vagrant-project-1 podem-se aceder às possíveis configurações, descomentando o código a baixo, vamos instalar o apache 2:

    config.vm.provision "shell", inline: <<-SHELL
         apt-get update
         apt-get install -y apache2
       SHELL

Para atualizar a maquina virtual com o apache temos que digitar:

    vagrant halt
    
    vagrant up –-provision 

para podermos aceder ao servidor local temos de descomentar e alterar o código a baixo na vagrantfile:

    config.vm.network "forwarded_port", guest: 80, host: 8010

Para podermos aceder a uma pasta que foi Criada na pasta do projecto e nomeada html, onde foi colocado um ficheiro índex.html com o conteudo html que quisermos, temos que descomentar e alterar o código a baixo na vagrantfile:

    config.vm.synced_folder "./html", "/var/www/html"

para atualizar a VM, na linha de comando fazer:
 
    Vagrant reload

Colocando no browser **http://localhost:8010/** vamos ter o conteúdo da nossa pasta

Para destruir a VM digita-se:

    vagrant destroy –f 

E para remover a box dos ficheiros locais:

    vagrant box remove envimation/ubuntu-xenial 


projectos do CA2-part1 e part2
-----

Com uma sessão iniciada no virtual box, e para termos todas as condições para os projectos correrem, temos instalar o git, java, gradle e maven com os comandos:

    sudo apt install git
    
    sudo apt install openjdk-8-jdk-headless
    
    sudo apt install gradle
        
    sudo apt install maven

De seguida proceder ao clone do repositório com o comando:

    git clone https://luisfvfigueiredo@bitbucket.org/luisfvfigueiredo/devops-19-20-b-1091064.git
	
Vamos inicializar então na pasta **~/devops-19-20-b-1091064/CA2/part1**  projecto Gradle criado no CA2-part1 com os seguintes comandos:

    ./gradlew build
    
    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

A parte gráfica deste projecto não pode ser acedida pelo Linux Ubuntu. Para fazermos o ruCliente temos que abrir o Intelij, e alterar o ficheiro build.glradle para:

    task runClient(type:JavaExec, dependsOn: classes){
        group = "DevOps"
        description = "Launches a chat client that connects to a server on localhost:59001 "
      
        classpath = sourceSets.main.runtimeClasspath
    
        main = 'basic_demo.ChatClientApp'
    
        args '192.168.56.100', '59001'
    }
    
Num terminal do nosso OS externo à VM podemos aceder a pasta **~/devops-19-20-b-1091064/CA2/part1** e digitar:

    ./gradlew runClient
	
Se quisermos inicializar o projecto da CA2-part2, na pasta **~/devops-19-20-b-1091064/CA2/part2/demo** digitamos os seguintes comandos:

    ./gradlew build
    
    ./gradlew bootRun

A parte gráfica deste projecto não pode ser acedida pelo Linux Ubuntu. temos de ir a um browser e aceder por **http://192.168.56.100:8010/**