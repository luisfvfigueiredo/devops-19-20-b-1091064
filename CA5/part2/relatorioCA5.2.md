

CA5 - DeVops Class Assignment 5 - part 2 - readme file
===================

Pipeline de integração do projecto demo do CA2-part2
-----
Feita a introdução e construído um primeiro Pipeline vamos agora adaptar este processo ao nosso projecto do CA2-part2, executando mais alguns stages que ainda não tínhamos visto.

Implementação dos Stages
-----
Comecei por criar um novo Pipeline Job no Jenkins e nomeei-o de "CA5-part2" e nas configurações defini o Pipeline script como SCM, onde coloquei o meu repositório **https://luisfvfigueiredo@bitbucket.org/luisfvfigueiredo/devops-19-20-b-1091064.git** , defini como credenciais as criadas no CA2-part2 e como Sript Path o caminho para a pasta demo, onde será colocada a Jenkinsfile (CA2/part2/demo/Jenkinsfile).
Para que o Gradle não de problemas de privilégios de administrador, acrescentei **`id 'base'`** ao ficheiro build.gradle.

Feito isto vamos começar por colocar uma Jenkinsfile na parta demo e começar a criar as Stages do enunciado:

#### Checkout Stage

Para fazer o checkout do meu repositorio e estruturar o pipe line fiz o script a baixo, onde fornecemos as credenciais e o caminho para o repositório no bitbucket.

        pipeline {
            agent any
        
            stages {
                stage('Checkout') {
                    steps {
                        echo 'Checking out...'
                        git credentialsId: 'luisfvfigueiredo-bitbucket', url: 'https://bitbucket.org/luisfvfigueiredo/devops-19-20-b-1091064/'
                    }
                }
            }
        }

#### Assemble Stage

No Stage Assemble é usado o **./gradlew assemble** em vez do **./gradlew build** para que não sejam para já executados os testes.

            stage('Assemble') {
                steps {
                    echo 'Assembling...'
                    sh 'cd CA2/part2/demo && ./gradlew clean assemble'
                }
            }
            
#### Test Stage

No Stage Test são executados todos os testes que estejam disponivei atravez do comando **./gradlew test** e corrido o relatório do gerado pelo Junit para estes testes.

            stage('Test') {
                steps {
                    echo 'Testing...'
                    sh 'cd CA2/part2/demo && ./gradlew test'
                    junit 'CA2/part2/demo/build/test-results/test/*.xml'
                }
            }
            
#### Javadoc Stage

No Stage Javadock é gerado o Javadock do projeto através do comando **gradle javadoc** e publicado o ficheiro gerado(para termos o publishHTML temos de ter instalado o plugin do mesmo).
            
            stage('Javadoc') {
                steps {
                    echo 'Generating Javadoc...'
                    sh 'cd CA2/part2/demo && gradle javadoc'
                    publishHTML (target : [allowMissing: false, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'CA2/part2/demo/build/docs/javadoc', reportFiles: 'index.html', reportName: 'My Reports', reportTitles: 'The Report'])
                }
            }
            
#### Archive Stage

No Stage Archive e guardado o ficheiro .war gerado durante o Assemble.
            
            stage('Archive') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'CA2/part2/demo/build/libs/*'
                }
            }
           
#### Pubish Image Stage

Neste stage vamos criar um Docker no Docker Hub com o tag **ca5-part2** , dando o caminho para a Dorckerfile que foi criada.
                        
            stage('Pubish Image') {
                steps {
                    echo 'Publishing docker image...'
                    script{
                        docker.build("luisfvfigueiredo/ca5-part2:${env.BUILD_ID}", "CA2/part2/demo").push()
                    }
                }
            }
            
Vai a baixo o Script do Dockerfile criado na pasta demo:

    FROM tomcat
    
    RUN apt-get update -y
    
    RUN apt-get install -f
    
    RUN apt-get install git -y
    
    RUN apt-get install nodejs -y
    
    RUN apt-get install npm -y
    
    RUN mkdir -p /tmp/build
    
    WORKDIR /tmp/build/
    
    RUN git clone https://luisfvfigueiredo@bitbucket.org/luisfvfigueiredo/devops-19-20-b-1091064.git
    
    WORKDIR /tmp/build/devops-19-20-b-1091064/CA2/part2/demo/
    
    RUN ./gradlew clean build
    
    EXPOSE 8080


Para finalizar, podemos fazer o Build Now do Pipeline com o repositório já atualizado no Bitbucket. Feito isto, conseguimos já encontrar o Docker em **https://hub.docker.com/repository/docker/luisfvfigueiredo/ca5-part2**

