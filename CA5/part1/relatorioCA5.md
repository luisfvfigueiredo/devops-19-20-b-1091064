

CA5 - DeVops Class Assignment 5 - part 1 - readme file
===================

Introdução ao Jenkins
-----
Jenkins é um servidor de Integração Contínua open-source feito em Java, pode ser rodado de forma standalone ou como uma aplicação web dentro de um servidor web.

As suas vantagens são, os Builds periódicos, testes automatizados, possibilita análise de código, identifica erros mais cedo, é fácil de operar e configurar, proporciona uma comunidade ativa e integra outras ferramentas através de plugins existentes na própria aplicação.

Instalção e Pipelines
-----

Comecei por fazer o dowload do ficheiro .zip **jenkins.war** em: https://www.jenkins.io/download . Numa linha de comando inicializada na pasta onde foi colocada a pasta zip jenkins.war corri o seguinte comando para inicializar o Jenkins:

    java -jar jenkins.war 

No correr da instalação foi gerada uma password para continuar a instalação, para ser colocada de seguida no browser em http://localhost:8080/ para proceder à continuação da instalação.


Já depois de finalizado o processo de instalação, foi criado o primeiro pipeline em "configurar", acrescentando o script fornecido em https://gist.github.com/atb/bb88c4231f095a8bd8566b5dab415850 :

    pipeline {
        agent any
    
        stages {
            stage('Build') {
                steps {
                    echo 'Building...'
                    script {
                        if (isUnix())
                            sh 'java -version'
                        else
                            bat 'java -version'
                    }
                }
            }
            stage('Test') {
                steps {
                    echo 'Testing...'
                }
            }
        }
    }

De seguida foi feito o "Build Now".

Entrando no Pipeline a ser construido e acedendo ao "Console" podemos ver o progresso.

Fizemos um segundo Pipeline com o projecto de chat original com o script em https://gist.github.com/atb/d23ce58350c4df272473c7fd7a96838f 

    pipeline {
        agent any
    
        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git 'https://bitbucket.org/luisnogueira/gradle_basic_demo'
                }
            }
            stage('Build') {
                steps {
                    echo 'Building...'
                    sh './gradlew clean build'
                }
            }
            stage('Archiving') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }


Em "Credentials/Sistem/Global_Credentials" criei uma credencial para acesso ao meu bitbucket com a respetiva passe e dei-lhe o ID de luisfvfigueiredo-bitbucket. De seguida construi um Pipeline, desta vez do tipo "Script from SCM", definindo como SCM o git, usando o meu repositório privado com o programa basic (https://luisfvfigueiredo@bitbucket.org/luisfvfigueiredo/gradle_basic_demo.git) e dando a password criada previamente(luisfvfigueiredo-bitbucket).
A este repositório já tinha sido adicionada a jenkinsfile alterada a baixo e que vai anexa a este readme file.

    pipeline {
        agent any
    
        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'luisfvfigueiredo-bitbucket', url: 'https://luisfvfigueiredo@bitbucket.org/luisnogueira/gradle_basic_demo.git'
                }
            }
            stage('Build') {
                steps {
                    echo 'Building...'
                    sh './gradlew clean build'
                }
            }
            stage('Archiving') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }


Para entrar novamente no jenkins depois de para de o executar foi-me pedida a password inicial, que encontrei em:
    
    Users/Luis/.jenkins/initialAdminPassword

