

CA2 - DeVops Class Assignment 2 part 1 - readme file
===================

Gradle é um sistema de automatização de builds, assim como o Ant e Maven.

Os arquivos de configuração são chamados de build.gradle, e são arquivos de texto simples que usam a sintaxe da linguagem Groovy para configurar a compilação com os elementos fornecidos pelo plugin do Gradle. Na maioria dos casos, só é necessário editar os arquivos de configuração de cada modulo.

Após esta breve introdução, está descrita a instalação do Gradle, onde foram executados os passos a baixo para correr a applicação de chat.


Inicialização do programa em Gradle
-----

Entrar no terminal no IntelliJ e aceder à pasta:

    /Users/Luis/IdeaProjects/devops-19-20-b-1091064/CA2/part1/ 
    
correr o built do programa:
    
     ./gradlew build
    
No mesmo terminal foi inicializado o progrma de chat com o comando:
    
    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
    
Em linhas de comando do sistema foram inicializadas 2 janelas de chat, acedendo à pasta:

    /Users/Luis/IdeaProjects/devops-19-20-b-1091064/CA2/part1/
    
Correr o comando:

    ./gradlew runClient
    
Este comando inicializará uma nova janela onde se digita o nome do usuário e desta forma está concluida a inicialização do chat.


Criação de Tasks
-----

Para criaçao de novas tasks é necessário modificar o build.gradle, acrescentando-as ao seu código. A baixo irei descrever as tasks pedidas:

Execute The Server - esta task ira executar o servidor do programa de chat.


    task runServer(type:JavaExec, dependsOn: classes){
        group = "DevOps"
        description = "Launches the chat server on localhost:59001 "
    
        classpath = sourceSets.main.runtimeClasspath
    
        main = 'basic_demo.ChatServerApp'
    
        args '59001'
    }
    
Copy - Após criação da pasta "backupFile" na raiz do projecto a task a baixo irá executar uma copia do conteudo do Source para a mesma.

      task copyTask(type:Copy){
           from 'src'
           into 'backUpFile'
      }

Zip - Após criação da pasta "zipFile" na raiz do projecto a task a baixo irá executar uma copia do conteudo do Source, comprimindo-o no formato .zip, para a mesma.

     task zipTask(type:Zip){
         from 'src'
         archiveName 'zipFile.zip'
         destinationDir(file('zipFile'))
     }