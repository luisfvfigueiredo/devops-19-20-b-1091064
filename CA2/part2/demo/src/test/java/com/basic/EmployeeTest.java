package com.basic;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @org.junit.jupiter.api.Test
    void EmployeeConstructor() {
        //Arrange,Act,Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee("Frodo", "Baggins", null); });
    }

}